from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from lxml import etree
from selenium.webdriver.firefox.options import Options
import time
import json
import sys

options = Options()
options.headless = True
options.proxy={
                        "http": "95.68.115.20:5328",
                        "https": "95.68.115.202:53281"
                }

driver = webdriver.Firefox(options=options)

urlsfile = sys.argv[1]
outpath = sys.argv[2]

with open(urlsfile, "r") as f:
    urls = [ json.loads(l) for l in f ]

for url in urls:
    if url.startswith("/video"):
        continue

    driver.get("https://www.scmp.com" + url)
    curre_url = driver.current_url

    print('getting', curre_url)
    
    driver.find_element_by_css_selector('body').click()
    html = driver.get_attribute('innerHTML')
    body = driver.find_element_by_css_selector('body')
    driver.implicitly_wait(20)
    
    while True:
        doc = etree.HTML(driver.page_source)
        time.sleep(0.01)
        body.send_keys(Keys.DOWN)
        new_driver_url = driver.current_url
        if curre_url != new_driver_url:
            break
        
    title = doc.xpath("//h3[@class='info__headline headline']//text()")[0]
    explanation = doc.xpath("//li[@class='generic-article__summary--li content--li']//text()")[0:2]
    all_docs = doc.xpath("//*[@class='generic-article__body article-details-type--p content--p']//text()")
    
    fname = curre_url.replace("://", "___")
    fname = fname.replace("/", "_")
    
    with open( "../news/" + fname, "w+") as f:
        f.write(title)
        for item in all_docs:
            f.write(item +"\n")
            
