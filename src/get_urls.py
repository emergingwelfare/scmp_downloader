import json
import requests
import time
import send_mail
import traceback
from re import findall

proxy_worked=None

def get_IPs():
    r = requests.get('https://www.sslproxies.org/')                                                                                                                          

    matches = findall(r"<td>\d+.\d+.\d+.\d+</td><td>\d+</td>", r.text)                                                                                                       

    revised = [m.replace('<td>', '') for m in matches]                                                                                                                       

    sockets = [s[:-5].replace('</td>', ':') for s in revised]                                                                                                                

    return sockets


url = "https://apigw.scmp.com/search/v1"

querystring = {"q":"[QUERY]+more:pagemap:metatags-cse_type:Article",
    "offset":"1",
    "limit":"10",
    "apikey":"MyYvyg8M9RTaevVlcIRhN5yRIqqVssNY"}

headers = {
    'User-Agent': "PostmanRuntime/7.20.1",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "543fc3e5-04b5-488d-ba9b-b17c8b681ff7,65f71be3-6fd9-41e8-919a-9e46b0284ff9",
    'Host': "apigw.scmp.com",
    'Accept-Encoding': "gzip, deflate",
    'Cookie': "__cfduid=dfd019025b6e7493bc6a38de598f8f3491577692794",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }
    
sockets=get_IPs()
sum_of_news_downloaded=0
with open("random_query_words.txt", "r" ,encoding="utf-8") as fi:
    query_words = fi.read().splitlines()

def proxy_request(url=url, headers=headers, params=querystring):
    global proxy_worked
    try: 
        r_test=requests.request("GET",url=url, headers=headers, params=querystring,proxies={"https":"https://"+proxy_worked,"'http'":'http://'+proxy_worked},timeout=3.0) 
        if r_test.status_code==200: 
            proxy_worked=x
            return r_test
    except Exception as e: 
        pass  

    for x in sockets: 
        try: 
            r_test=requests.request("GET",url=url, headers=headers, params=querystring,proxies={"https":"https://"+x,"'http'":'http://'+x},timeout=3.0) 
            if r_test.status_code==200: 
                proxy_worked=x
                return r_test
        except Exception as e: 
            pass


for j, w in enumerate(query_words[0:1000]):
    if j < 497 or j>600:
        continue
    query = {"q":"[QUERY]+more:pagemap:metatags-cse_type:Article"}
    querystring["q"] = query["q"].replace("[QUERY]",w)
    fname = u'crawled_urls/231219_keyword_' + str(j) + ".jl"
    print('getting', w.encode('utf-8'), fname)
    with open(fname, "w",encoding="utf-8") as fo:
        for i in range(1, 100000,10):
            querystring["offset"] = str(i)
            time.sleep(10)
            try:
                r=proxy_request(params=querystring)
                if not r:
                    #print("json were empty\n",querystring)
                    send_mail.send_email("aalabrash18@ku.edu.tr","SCMP_Downloading","json were empty\n"+str(w.encode("utf-8"))+"\t"+str(i))
                    break
                urls = json.loads(r.text)["items"]
                if len(urls) == 0:
                    #print("url==0\n",querystring)
                    send_mail.send_email("aalabrash18@ku.edu.tr","SCMP_Downloading","url==0\n"+str(w.encode("utf-8"))+"\t"+str(i))
                    break
                
                for u in urls:
                    [u.pop(x, None) for x in ["authors","squareImage","snippet"]] # get ride of unnecessary items
                    fo.write(json.dumps(u, ensure_ascii=False) + "\n")
                    sum_of_news_downloaded+=1
                if sum_of_news_downloaded%1000==0:
                    send_mail.send_email("aalabrash18@ku.edu.tr","SCMP_Downloading","{} are downloaded".format(sum_of_news_downloaded))
            except Exception as e:
                print('error: ',traceback.format_exc(),str(w.encode("utf-8"))+"\t"+str(i))
                send_mail.send_email("aalabrash18@ku.edu.tr","SCMP_Downloading","an error occurred {0} with the following query string{1}\n".format(traceback.format_exc(),str(w.encode("utf-8"))+"\t"+str(i)))

send_mail.send_email("aalabrash18@ku.edu.tr","SCMP_Downloading","ALL DONE")