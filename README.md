# scmp_downloader

This repo includes the required code and scripts to get scmp articles urls and download them.

`random_query_words.txt` file contains ~10K words taken from our scmp data, where vocab set was extracted and vocabs with min count of 10 were kept in the list.

`get_urls.py` file queries scmp search api (Elastic Search) using the first 435 words in `random_query_words.txt` (those words are not ordered), each query returns up to 9950 urls per query, these urls are written then to files in the folder crawled_urls/

